using System.Collections;
using Project.Scripts;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Project
{
    public class GameManager : MonoBehaviour
    {
        public  static  GameManager Instance { get; private set; }
    
        [SerializeField] private float timeSpawn; //время спавна
        [SerializeField] private float spawnPosY; //Позиция спавна по y
        [SerializeField] private float spawnPosMinX; //Минимальная позиция спавна по X
        [SerializeField] private float spawnPosMaxX; //Максимальная позиция спавна по X
        [SerializeField] private float minSpeed; //Минимальная скорость
        [SerializeField] private float maxSpeed; //Максимальная скорость
        [SerializeField] private GameObject prefab; //Префаб для спавна
        [SerializeField] private Text text; //Ссылка на текст
        [SerializeField] private float time;//Время игры
        [SerializeField] private AudioClip din;
        private GameObject player;
        private bool game;
        private int score;
    
        private AudioSource audioSource;

        public void Start()
        {
            player = GameObject.FindGameObjectWithTag("MainCamera");
            audioSource = player.GetComponent<AudioSource>();
            Instance = this;
            score = 0;
            text.text = "0";
            game = true;
            StartCoroutine(SpawnCycle());
            StartCoroutine(ExecuteAfterTime(time));
        }

        IEnumerator ExecuteAfterTime(float timeInSec)
        {
            yield return new WaitForSeconds(timeInSec);
        
            SceneManager.LoadScene(0);
        }
    
        private IEnumerator SpawnCycle()
        {
            while (game)
            {
                yield return new WaitForSeconds(timeSpawn);
                SpawnStart();  
            }
        }

        private void SpawnStart()
        {
            GameObject star = Instantiate(prefab, Vector3.zero, Quaternion.identity);
            star.GetComponent<StarModel>().Init(new Vector2(Random.Range(spawnPosMinX, spawnPosMaxX), spawnPosY), 
                Random.Range(minSpeed, maxSpeed));
        }

        public void DestroyStar(GameObject star, bool add)
        {
            Destroy(star);
            if (add)
            {
                score++;
                audioSource.PlayOneShot(din);
            }

            text.text = score.ToString();
            if (PlayerPrefs.GetInt("MaxScore") < score)
            {
                PlayerPrefs.SetInt("MaxScore", score);
            }
        }

    }
}
