﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Project.Scripts
{
    public class Paused : MonoBehaviour
    {
        [SerializeField] GameObject pause;
        public static bool stop;
        void Start()
        {
            pause.SetActive(false);
            stop = false;
        }

        // Update is called once per frame
        void Update()
        {
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                pause.SetActive(true);
                stop = true;
                Time.timeScale = 0;
            }
        }

        public void PauseOff()
        {
            pause.SetActive(false);
            stop = false;
            Time.timeScale = 1;
        }

        public void Menu()
        {
            SceneManager.LoadScene(0);
            stop = false;
            Time.timeScale = 1;
        }
    }
}
