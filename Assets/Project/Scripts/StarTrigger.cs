using UnityEngine;

namespace Project.Scripts
{
    public class StarTrigger : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D col)
        {
            if (col.CompareTag("Good"))
                GameManager.Instance.DestroyStar(col.gameObject, false);
        }
    }
}
