using UnityEngine;
using Random = UnityEngine.Random;

namespace Project.Scripts
{
    public class StarModel : MonoBehaviour
    {
        private float speedz;

        private void Start()
        {
            speedz = Random.Range(100f, 190f);
        }

        public void Init(Vector2 position, float speed)
        {
            transform.position = new Vector3(position.x, position.y, 0); //Задание позиции
            GetComponent<Rigidbody2D>().AddForce(Vector2.down * speed * 10); //Добавление силы вниз
        }

        private void Update()
        {
            transform.Rotate(Vector3.forward*speedz*Time.deltaTime);
        }

        private void OnMouseDown()
        {
            if(Paused.stop == false) 
                GameManager.Instance.DestroyStar(gameObject, true);
        }
    
    }
}
