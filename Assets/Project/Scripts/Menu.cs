﻿using UnityEngine;
using UnityEngine.UI;

namespace Project.Scripts
{
    public class Menu : MonoBehaviour
    {
        public Text MaxScore;

        private void Start()
        {
            MaxScore.text = "Max Score: " + PlayerPrefs.GetInt("MaxScore");
        }

        public void Update()
        {
            MaxScore.text = "Max Score: " + PlayerPrefs.GetInt("MaxScore");
        }
    }
}
